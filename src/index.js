require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const v1MovieRouter = require("./v1/routes/movieRoutes");
const PORT = process.env.PORT || 3000;
const mongoString = process.env.DATABASE_URL;

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect(mongoString);
  const database = mongoose.connection;
  database.on("error", (error) => {
    console.log(error);
  });
  database.once("connected", () => {
    console.log("Database Connected");
  });

  const app = express();
  app.use(express.json());
  app.use("/api/v1/movies", v1MovieRouter);
  app.listen(PORT, () => {
    console.log(`🚀 Server listening on port ${PORT}`);
  });
}
