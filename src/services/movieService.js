const MovieRepository = require("../database/MovieRepository");

const getAllMovies = (filterParams) => {
  try {
    const allMovies = MovieRepository.getAllMovies(filterParams);
    return allMovies;
  } catch (error) {
    throw error;
  }
};

const getOneMovie = (movieId) => {
  try {
    const movie = MovieRepository.getOneMovie(movieId);
    return movie;
  } catch (error) {
    throw error;
  }
};

const createNewMovie = (newMovie) => {
  try {
    const createdMovie = MovieRepository.createNewMovie(newMovie);
    return createdMovie;
  } catch (error) {
    throw error;
  }
};

const updateOneMovie = (movieId, changes) => {
  try {
    const updatedMovie = MovieRepository.updateOneMovie(movieId, changes);
    return updatedMovie;
  } catch (error) {
    throw error;
  }
};

const deleteOneMovie = (movieId) => {
  try {
    MovieRepository.deleteOneMovie(movieId);
  } catch (error) {
    throw error;
  }
};

module.exports = {
  getAllMovies,
  getOneMovie,
  createNewMovie,
  updateOneMovie,
  deleteOneMovie,
};
