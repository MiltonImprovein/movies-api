const DB = require("./db.json");
const { saveToDatabase } = require("./utils");
const Movie = require("../models/movie");

/**
 * @openapi
 * components:
 *   schemas:
 *     Movie:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           example: 61dbae02-c147-4e28-863c-db7bd402b2d6
 *         name:
 *           type: string
 *           example: Tommy V
 *         mode:
 *           type: string
 *           example: For Time
 *         equipment:
 *           type: array
 *           items:
 *             type: string
 *           example: ["barbell", "rope"]
 *         exercises:
 *           type: array
 *           items:
 *             type: string
 *           example: ["21 thrusters", "12 rope climbs, 15 ft", "15 thrusters", "9 rope climbs, 15 ft", "9 thrusters", "6 rope climbs, 15 ft"]
 *         createdAt:
 *           type: string
 *           example: 4/20/2022, 2:21:56 PM
 *         updatedAt:
 *           type: string
 *           example: 4/20/2022, 2:21:56 PM
 *         trainerTips:
 *           type: array
 *           items:
 *             type: string
 *           example: ["Split the 21 thrusters as needed", "Try to do the 9 and 6 thrusters unbroken", "RX Weights: 115lb/75lb"]
 */
const getAllMovies = async (filterParams) => {
  try {
    const { year } = filterParams;
    let data = year ? await Movie.find({ year }) : await Movie.find();
    return data;
  } catch (error) {
    throw { status: 500, message: error };
  }
};

const getOneMovie = (movieId) => {
  try {
    const movie = DB.movies.find((movie) => movie.id === movieId);

    if (!movie) {
      throw {
        status: 400,
        message: `Can't find movie with the id '${movieId}'`,
      };
    }

    return movie;
  } catch (error) {
    throw { status: error?.status || 500, message: error?.message || error };
  }
};

const createNewMovie = async (newMovie) => {
  try {
    const isAlreadyAdded =
      DB.movies.findIndex((movie) => movie.title === newMovie.title) > -1;

    if (isAlreadyAdded) {
      throw {
        status: 400,
        message: `Movie with the title '${newMovie.title}' already exists`,
      };
    }
    const data = new Movie({
      title: newMovie.title,
      description: newMovie.description,
      duration: newMovie.duration,
      year: newMovie.year,
    });
    const dataToSave = await data.save();

    return dataToSave;
  } catch (error) {
    throw { status: 500, message: error?.message || error };
  }
};

const updateOneMovie = (movieId, changes) => {
  try {
    const isAlreadyAdded =
      DB.movies.findIndex((movie) => movie.name === changes.name) > -1;

    if (isAlreadyAdded) {
      throw {
        status: 400,
        message: `Movie with the name '${changes.name}' already exists`,
      };
    }

    const indexForUpdate = DB.movies.findIndex((movie) => movie.id === movieId);

    if (indexForUpdate === -1) {
      throw {
        status: 400,
        message: `Can't find movie with the id '${movieId}'`,
      };
    }

    const updatedMovie = {
      ...DB.movies[indexForUpdate],
      ...changes,
      updatedAt: new Date().toLocaleString("en-US", { timeZone: "UTC" }),
    };

    DB.movies[indexForUpdate] = updatedMovie;
    saveToDatabase(DB);

    return updatedMovie;
  } catch (error) {
    throw { status: error?.status || 500, message: error?.message || error };
  }
};

const deleteOneMovie = (movieId) => {
  try {
    const indexForDeletion = DB.movies.findIndex(
      (movie) => movie.id === movieId
    );
    if (indexForDeletion === -1) {
      throw {
        status: 400,
        message: `Can't find movie with the id '${movieId}'`,
      };
    }
    DB.movies.splice(indexForDeletion, 1);
    saveToDatabase(DB);
  } catch (error) {
    throw { status: error?.status || 500, message: error?.message || error };
  }
};

module.exports = {
  getAllMovies,
  getOneMovie,
  createNewMovie,
  updateOneMovie,
  deleteOneMovie,
};
