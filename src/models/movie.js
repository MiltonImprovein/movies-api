const mongoose = require('mongoose');
const {Schema, model} = mongoose
const movieSchema = new Schema({
    title: {
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    duration: {
        required: true,
        type: String
    },
    year: {
        required: true,
        type: String
    },
})
movieSchema.set('toJSON', {
    transform: (document,returnedObject)=>{
        returnedObject.id = returnedObject._id
        delete returnedObject._id,
        delete returnedObject.__v
    }
})
module.exports = model('Movie', movieSchema)