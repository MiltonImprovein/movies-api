const movieService = require("../services/movieService");

const getAllMovies = async (req, res) => {
  const { year } = req.query;
  try {
    const allMovies = await movieService.getAllMovies({ year });
    res.send({ status: "OK", data: allMovies });
  } catch (error) {
    res
      .status(error?.status || 500)
      .send({ status: "FAILED", data: { error: error?.message || error } });
  }
};

const getOneMovie = (req, res) => {
  const {
    params: { movieId },
  } = req;

  if (!movieId) {
    res.status(400).send({
      status: "FAILED",
      data: { error: "Parameter ':movieId' can not be empty" },
    });
    return;
  }

  try {
    const movie = movieService.getOneMovie(movieId);
    res.send({ status: "OK", data: movie });
  } catch (error) {
    res
      .status(error?.status || 500)
      .send({ status: "FAILED", data: { error: error?.message || error } });
  }
};

const createNewMovie = async (req, res) => {
  const { body } = req;
  if (!body.title || !body.description || !body.duration || !body.year) {
    res.status(400).send({
      status: "FAILED",
      data: {
        error:
          "One of the following keys is missing or is empty in request body: 'title', 'description', 'duration', 'year'",
      },
    });
  }

  const newMovie = {
    title: body.title,
    description: body.description,
    duration: body.duration,
    year: body.year,
  };

  try {
    const createdMovie = await movieService.createNewMovie(newMovie);
    console.log("createdMovie: ", createdMovie);
    res.status(201).send({ status: "OK", data: createdMovie });
  } catch (error) {
    res
      .status(error?.status || 500)
      .send({ status: "FAILDED", data: { error: error?.message || error } });
  }
};

const updateOneMovie = (req, res) => {
  const {
    body,
    params: { movieId },
  } = req;

  if (!movieId) {
    res.status(400).send({
      status: "FAILED",
      data: { error: "Parameter ':movieId' can not be empty" },
    });
  }

  try {
    const updatedMovie = movieService.updateOneMovie(movieId, body);
    res.send({ status: "OK", data: updatedMovie });
  } catch (error) {
    res
      .status(error?.status || 500)
      .send({ status: "FAILED", data: { error: error?.message || error } });
  }
};

const deleteOneMovie = (req, res) => {
  const {
    params: { movieId },
  } = req;

  if (!movieId) {
    res.status(400).send({
      status: "FAILED",
      data: { error: "Parameter ':movieId' can not be empty" },
    });
  }

  try {
    movieService.deleteOneMovie(movieId);
    res.status(204).send({ status: "OK" });
  } catch (error) {
    res
      .status(error?.status || 500)
      .send({ status: "FAILED", data: { error: error?.message || error } });
  }
};

module.exports = {
  getAllMovies,
  getOneMovie,
  createNewMovie,
  updateOneMovie,
  deleteOneMovie,
};
