const express = require("express");
const router = express.Router();
const movieController = require("../../controllers/movieController");
router
  .get("/", movieController.getAllMovies)
  .get("/:movieId", function (req, res) {
    res.send(`Get movie ${req.params.movieId}`);
  })
  .post("/", movieController.createNewMovie)
  .delete("/:movieId", function (req, res) {
    res.send(`Delete movie ${req.params.movieId}`);
  });

module.exports = router;
